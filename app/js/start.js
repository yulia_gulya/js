import dynamicCards from './dynamicCards.js';
import filter from './filter.js';
import newYear from './newYear.js';

let xhr = new XMLHttpRequest();
xhr.open("GET", "http://localhost:3000/api/dogs.json", true);
xhr.send();
xhr.onload = () => {
    let animalsInBasket = [];
    let newList = [];
    let costAnimals = 0;

    if (document.location.href === "http://localhost:3000/") {
        sessionStorage.setItem("filter", JSON.stringify(JSON.parse(xhr.response).dogs));
    }

    dynamicCards(JSON.parse(xhr.response).dogs);

    document.querySelector(".filter").querySelectorAll("input").forEach(el => {
        el.addEventListener('click', evt => {
                newList = filter(JSON.parse(xhr.response).dogs);
                sessionStorage.setItem("filter", JSON.stringify(newList));
                document.querySelector(".popup button").addEventListener("click", evt => {
                    dynamicCards(JSON.parse(sessionStorage.getItem("filter")));
                })
                document.querySelector(".popup").style.display = 'block';
                document.querySelector(".popup .count").innerText = newList.length;
            }
        )
    });

    document.querySelectorAll(".add-basket-button").forEach(el => {
        el.addEventListener("click", evt => {
            animalsInBasket.push(evt.currentTarget.parentNode.querySelector(".product-list__name span").textContent);
            costAnimals += Number(evt.currentTarget.parentNode.querySelector(".product-list__cost span").textContent.slice(0, -1).split(' ').join(''));
            document.querySelector(".basket-in-header").innerHTML = "(" + animalsInBasket.length + ")" + " - " + costAnimals;
            localStorage.setItem("inBasket", animalsInBasket);
        })
    })

/*
    document.querySelector(".new-year").style.display = 'block';
    document.querySelector(".new-year").addEventListener("click", evt => {
        newYear();
    })
*/
}









