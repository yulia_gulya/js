let xhr = new XMLHttpRequest();
xhr.open("GET", "http://localhost:3000/api/dogs.json", true);
xhr.send();
xhr.onload = () => {
    let animalsInBasket = [];
    animalsInBasket.push(localStorage.getItem("inBasket").split(","));

    let basket = new Basket();
    let newArray = basket.parseData(animalsInBasket.flat(), JSON.parse(xhr.response).dogs);

    basket.allAnimals(newArray);
    basket.getSum(newArray);
    basket.getQuantity(newArray);
}

class Basket {

    constructor(count, price) {
        this.count = count;
        this.price = price;
    }

    parseData(animalsInBasket, allAnimals) {
        let newData = [];
        for (let i = 0; i < animalsInBasket.length; i++) {
            newData[i] = allAnimals.filter(el => el.name === animalsInBasket[i]);
        }
       return newData.flat();
    }

    allAnimals(animalsData) {
        let template = document.querySelector('#basket__item');

        for (let i = 0; i < animalsData.length; i++) {
            let clone = template.content.cloneNode(true);
            clone.querySelector('.animal-photo').src = animalsData[i].photo;
            clone.querySelector('.animal-photo').alt = animalsData[i].name;
            clone.querySelector('.basket__item .name').textContent = animalsData[i].name;
            clone.querySelector('.basket__item .price').textContent = animalsData[i].price;
            template.parentNode.appendChild(clone);
        }
    }

    getSum(animalsData) {
        let sum = 0;
        let result =  document.querySelector('.basket__result .sum');

        animalsData.forEach(el => {
            sum += Number(el.price.slice(0, -1).split(' ').join(''));
        });

        result.innerHTML = sum + "₽";
    }

    getQuantity(animalsData) {
        let result = document.querySelector('.basket__result .quantity');
        result.innerHTML = animalsData.length + " товара на сумму";
    }

}

if (document.querySelector("input[name = 'check-all']")) {
    document.querySelector("input[name = 'check-all']").addEventListener("click", function () {
        document.querySelector("input[name = 'check-all']").checked ?
            document.querySelectorAll("input[name = 'checks']").forEach(el => el.checked = true) :
            document.querySelectorAll("input[name = 'checks']").forEach(el => el.checked = false);
    });
}







