export default function newYear() {

    let background = setInterval(back, 2000);
    let backgroundNone = setInterval(backNone, 2000);

    function back() {
        getRandomEl().style.backgroundColor = '#' + (Math.random().toString(16) + '000000').substring(2, 8).toUpperCase();
    }

    function backNone() {
        getRandomEl().style.backgroundColor = "";
    }

    function getRandomEl() {
        let el = document.querySelectorAll('*');
        let rand = Math.floor(Math.random() * el.length);
        return el[rand];
    }
}