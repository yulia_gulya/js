export default function dynamicCards(animalsData) {

    if (JSON.parse(sessionStorage.getItem("filter")) !== null) {
        animalsData = JSON.parse(sessionStorage.getItem("filter"));
    }

    let randomCards = animalsData.sort(() => Math.random() - 0.5);
    let template = document.querySelector('#product-card');

    for (let i = 0; i < randomCards.length; i++) {
        let clone = template.content.cloneNode(true);
        clone.querySelector('.image-wrapper').addEventListener('click', evt => {
            document.location.href = `http://localhost:3000/animalDetails.html?id-${randomCards[i].id}`;
        });
        let name = clone.querySelector('.product-list__name').querySelector('span');
        let price = clone.querySelector('.product-list__cost').querySelector('span');
        clone.querySelector('img').src = randomCards[i].photo;
        clone.querySelector('img').alt = randomCards[i].name;
        name.textContent = randomCards[i].name;
        price.textContent = randomCards[i].price;
        template.parentNode.appendChild(clone);
    }
}
