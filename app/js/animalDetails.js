let xhr = new XMLHttpRequest();
xhr.open("GET", "http://localhost:3000/api/dogs.json", true);
xhr.send();
xhr.onload = () => {
    let newData = JSON.parse(xhr.response).dogs;

    let data = [];

    for (let i = 0; i < newData.length; i++) {
        if (newData[i].id === location.href.toString().split('-')[1]) {
            data = newData[i];
        }
    }

    let type = getTypesOrFeatures(data, JSON.parse(xhr.response).types);
    let feature = getTypesOrFeatures(data, JSON.parse(xhr.response).mainFeatures);

    animalDetails(data, type, feature);
}

function animalDetails(animalData, type, feature) {

    let template = document.querySelector('#animal-info');

    let clone = template.content.cloneNode(true);
    clone.querySelector('.path .name').textContent = animalData.name;
    clone.querySelector('img').src = animalData.photo;
    clone.querySelector('img').alt = animalData.name;
    clone.querySelector('.animal-details__name').textContent = animalData.name;
    clone.querySelector('.animal-details__value').textContent = animalData.price;
    clone.querySelector('.title').textContent = animalData.name;
    clone.querySelector('.size').textContent = animalData.size + " кг";
    clone.querySelector('.types').textContent = type.join(", ");
    clone.querySelector('.features').textContent = feature.join(", ");
    template.parentNode.appendChild(clone);
}

function getTypesOrFeatures(data, list) {

    let typeOrFeatureForAnimal = [];

    data.types.forEach((el) => {
        typeOrFeatureForAnimal.push(list.find(list => list.id === el).value);
    })

    return typeOrFeatureForAnimal;
}