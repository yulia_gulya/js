export default function filter(allAnimals) {

    let newArray = allAnimals;

    let types = filterTemp("type");
    let features = filterTemp("mainFeature");
    let minSize = document.querySelector(".filter .min");
    let maxSize = document.querySelector(".filter .max");

    filterTypes(types);
    filterFeatures(features);
    filterSize(minSize, maxSize);

    function filterTemp(type) {
        let array = document.querySelectorAll(`form input[name = "${type}"]:checked`);
        return [].map.call(array, obj => +obj.value)
    }

    function filterTypes(types) {
        newArray = newArray.filter(el => {
            return types.join('') === el['types'].filter(el => types.includes(el)).join('');
        });
    }

    function filterFeatures(features) {
        newArray = newArray.filter(el => {
            return features.join('') === el['mainFeatures'].filter(el => features.includes(el)).join('');
        });
    }

    function filterSize(min, max) {
        newArray = newArray.filter(el => {
            return el.size >= min.value && el.size <= max.value;
        });
    }

    return newArray;
}
